#!/bin/bash

echo "Installing JDK 8u144 for ARM 32 Bit."
sudo tar zxf jdk-8u144-linux-arm32-vfp-hflt.tar.gz
sudo mv ./jdk1.8.0_144/ /opt/
sudo ln -s /opt/jdk1.8.0_144/ /opt/jdk8
echo "JDK install now complete."
echo "Installing JavaFX Embedded SDK for armv6hf Version 8.60.9."
sudo unzip armv6hf-sdk-8.60.9.zip
sudo cp armv6hf-sdk/rt/lib/ext/jfxrt.jar /opt/jdk8/jre/lib/ext/
sudo cp armv6hf-sdk/rt/lib/arm/* /opt/jdk8/jre/lib/arm/
sudo cp armv6hf-sdk/rt/lib/javafx.platform.properties /opt/jdk8/jre/lib/
sudo cp armv6hf-sdk/rt/lib/javafx.properties /opt/jdk8/jre/lib/
sudo cp -r armv6hf-sdk/rt/lib/jfxswt.jar /opt/jdk8/jre/lib/ /opt/jdk8
sudo update-alternatives --install /usr/bin/java java /opt/jdk8/bin/java 1
sudo update-alternatives --set java /opt/jdk8/bin/java
sudo java -version
echo "Installation complete."
