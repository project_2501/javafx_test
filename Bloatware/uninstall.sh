#!/bin/bash

echo "Removing bloatware."

sudo rm -R /home/pi/python_games
sudo apt-get purge wolfram-engine
sudo apt-get purge bluej
sudo apt-get purge greenfoot
sudo apt-get purge nodered
sudo apt-get purge nuscratch
sudo apt-get purge scratch
sudo apt-get purge sonic-pi
sudo apt-get purge libreoffice
sudo apt-get purge claws-mail
sudo apt-get purgei18n
sudo apt-get purge minecraft-pi
python-pygame

echo "Bloatware removal complete."